<?php
  header('Access-Control-Allow-Origin: *');
  header('Content-type: application/json');
  include_once('../functions/abre_conexion.php');
  include_once('../functions/functions.php');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');
  $localIP = getHostByName(getHostName());

  $resultados = array();

  // "limpiamos" los campos del formulario de posibles códigos maliciosos
  $nom = mysqli_real_escape_string($mysqli, $_POST['nom']);
  $ape = mysqli_real_escape_string($mysqli, $_POST['ape']);
  $apm = mysqli_real_escape_string($mysqli, $_POST['apm']);
  $cel = mysqli_real_escape_string($mysqli, $_POST['cel']);
  $fat = mysqli_real_escape_string($mysqli, $_POST['fat']);
  $usuario_nombre = mysqli_real_escape_string($mysqli, $_POST['usr']);
  $usuario_email = mysqli_real_escape_string($mysqli, $_POST['mai']);
  $usuario_clave = mysqli_real_escape_string($mysqli, $_POST['pas']);

  $init_index = generateRandomString(8);
  $id_per = generateRandomString(8);
  $referencia = generateRandomString(9);
  $pagref = generateRandomRef(8);

  // comprobamos que el usuario ingresado no haya sido registrado antes
  $sql = $mysqli->query("SELECT nom FROM auth_table WHERE nom ='".$usuario_nombre."'");
  if ($sql->num_rows > 0) {
    $resultados[] = array("success"=> false, "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "error"=> "El usuario ya ha sido registrado previamente");
  } else {
    $sql = $mysqli->query("SELECT mai FROM perf_table WHERE mai ='".$usuario_email."'");
    if ($sql->num_rows > 0) {
      $resultados[] = array("success"=> false, "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "error"=> "El correo ya ha sido registrado previamente");
    } else {
      $usuario_clave = password_hash($usuario_clave, PASSWORD_BCRYPT); // encriptamos la contraseña ingresada con md5
      $sqlReg = $mysqli->query("INSERT INTO auth_table (nom, pas, init_index) VALUES ('".$usuario_nombre."', '".$usuario_clave."', '".$init_index."')");
      if($sqlReg) {
        // Guardamos el perfil de acuerdo al id padre
        $sqlId = $mysqli->query("SELECT id FROM auth_table WHERE init_index = '".$init_index."'");
        if ($sqlId->num_rows > 0) {
          $rowId = $sqlId->fetch_assoc();
          if ($sqlPerf = $mysqli->query("INSERT INTO perf_table (id_usr, lev, mai, nom, ape, apm, tel, cel, fec, ref, fat) VALUES ($rowId[id], 2, '".$usuario_email."', '".$nom."', '".$ape."', '".$apm."', '0000000000', $cel, '".$fechaActual."', '".$referencia."', '".$fat."')")) {
            // Guardamos el registro de pago y su referencia
            if ($sqlPag = $mysqli->query("INSERT INTO pag_table (id_usr, fla, ref, fec) VALUES ($rowId[id], 0, '" . $pagref . "', '0000-00-00 00:00:00')")) {
              // Guardamos el met table
              if ($sqlMet = $mysqli->query("INSERT INTO met_table (id_usr, usr, pas, srv, mon, rik) VALUES ($rowId[id], '', '', '', 0.0, 1)")) {
                $resultados[] = array("success"=> true, ""=> "Usuario Registrado", "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "feed_key"=> $init_index, "feed_user"=> $usuario_nombre);
              } else {
                $resultados[] = array("success"=> false, "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "error"=> "Error, Met ".mysqli_error($mysqli));
              }
            } else {
              $resultados[] = array("success"=> false, "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "error"=> "Error, Pag ".mysqli_error($mysqli));
              //$resultados[] = array("success"=> false, "error"=> mysqli_error($mysqli))
            }
          } else {
            $resultados[] = array("success"=> false, "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "error"=> "Error, Perf ".mysqli_error($mysqli));
          }
        } else {
          $resultados[] = array("success"=> false, "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "error"=> "Error, SeAuth ".mysqli_error($mysqli));
        }
      } else {
        $resultados[] = array("success"=> false, "type"=> "register", "ip"=> $localIP, "date"=> $fechaActual, "error"=> "Error, Auth ".mysqli_error($mysqli));
        //$resultados[] = array("success"=> false, "error"=> mysqli_error($mysqli));
      }
    }
  }

  print json_encode($resultados);
  // incluimos el archivo de desconexion a la Base de Datos
  include('../functions/cierra_conexion.php');
?>
