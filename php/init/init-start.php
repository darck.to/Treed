<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  
  $resultados = array();

  session_start();
  if ($_POST['a'] == 1) {
    $_SESSION['log'] = TRUE;
    $resultados[] = array("success"=> true, "message"=> "Sesion Iniciada");
  } else {
    $_SESSION['log'] = FALSE;
    session_destroy();
    $resultados[] = array("success"=> false, "message"=> "Sesion Cerrada");
  }
  print json_encode($resultados);
?>
