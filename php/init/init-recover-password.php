<?php
  header("Access-Control-Allow-Origin: *");
  header('Content-type: application/json');
  include_once('../functions/abre_conexion.php');
  include_once('../functions/functions.php');

  date_default_timezone_set("America/Mexico_City");
  $fechaActual = Date('Y-m-d H:i:s');
  $localIP = getHostByName(getHostName());

  $resultados = array();

  if ( empty($_POST['mai']) ) {
    echo "No has ingresado el usuario!";
  } else {
    $usuario_email = mysqli_real_escape_string($mysqli,$_POST['mai']);
    $sql = $mysqli->query("SELECT nom FROM perf_table WHERE mai = '$usuario_email'");
    if ($sql->num_rows > 0) {
      $row = $sql->fetch_assoc();
      $new_clave = generateRandomString(12); // asignamos el número de caracteres que va a tener la nueva contraseña
      $nueva_clave = password_hash($new_clave, PASSWORD_BCRYPT); // generamos una nueva contraseña de forma aleatoria
      $usuario_nombre = $row['nom'];
      // actualizamos los datos (contraseña) del usuario que solicitó su contraseña
      if ($mysqli->query("UPDATE auth_table SET pas = '$nueva_clave' WHERE nom = '$nom'")) {
        // Enviamos por email la nueva contraseña
        $remite_nombre = "Treed Inteligencia Financiera"; // Tu nombre o el de tu página
        $remite_email = "no-reply@treedmng.net"; // tu correo
        $asunto = "Sistema de Recuperacion de Password"; // Asunto (se puede cambiar)
        $mensaje = "A new password for username: <strong>".$usuario_nombre."</strong>. New password is: <strong>" . $new_clave . "</strong>.";
        $cabeceras = "From: ".$remite_nombre." <".$remite_email.">\r\n";
        $cabeceras = $cabeceras."Mime-Version: 1.0\n";
        $cabeceras = $cabeceras."Content-Type: text/html";
        $enviar_email = mail($usuario_email,$asunto,$mensaje,$cabeceras);
        if ($enviar_email) {
          $resultados[] = array("success"=>true, "type"=> "recover password", "ip"=> $localIP, "date"=> $fechaActual, "message"=>"Sent to: " . $usuario_email);
        } else {
          $resultados[] = array("success"=>false, "type"=> "recover password", "ip"=> $localIP, "date"=> $fechaActual, "message"=>"Error, contact support");
        }
      } else {
        $resultados[] = array("success"=>false, "type"=> "recover password", "ip"=> $localIP, "date"=> $fechaActual, "message"=>"Error, bdd error contact support");
        //printf("<br>Errormessage: %s\n", $mysqli->error);
      }
    } else {
      $resultados[] = array("success"=>true, "type"=> "recover password", "ip"=> $localIP, "date"=> $fechaActual, "message"=>"User not found");
    }
  }

  print json_encode($resultados);
  // incluimos el archivo de desconexion a la Base de Datos
  include('../functions/cierra_conexion.php');

?>
