<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    include_once('../functions/abre_conexion.php');

    $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
    $user = mysqli_real_escape_string($mysqli,$_POST['user']);

    $sql = $mysqli->query("SELECT id FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
    if ($sql->num_rows > 0) {
        $row = $sql->fetch_assoc();
        $sqlp = $mysqli->query("SELECT `pag_table`.`id_usr` as id, `perf_table`.`fec` as fea, `pag_table`.`fec` as fec, `perf_table`.`lev` as lev, `perf_table`.`nom` as nom, `perf_table`.`ape` as ape, `perf_table`.`apm` as apm, `perf_table`.`tel` as tel, `perf_table`.`cel` as cel, `perf_table`.`mai` as mai, `perf_table`.`ref` as ref FROM `perf_table` LEFT JOIN `pag_table` ON `pag_table`.`id_usr` = `perf_table`.`id_usr` WHERE `perf_table`.`id_usr` = '".$row['id']."'");
        if ($sqlp->num_rows > 0) {
            $rowp = $sqlp->fetch_assoc();
            $lev = $rowp['lev'];
            $fec = strtotime($rowp['fec']);
            $fec = date("d/m/y g:i A", $fec);
            $fec = date("d/m/y", strtotime($fec. ' + 1 years'));
            $fea = strtotime($rowp['fea']);
            $fea = date("d/m/y g:i A", $fea);
            // CFG file
            if (file_exists("../../cfg/menu.json")) {
                $fileJson = file_get_contents("../../cfg/menu.json");
                $json = json_decode($fileJson);
                foreach ($json as $key => $value) {
                    if ($key == "menu-" . $lev) {
                        foreach ($value as $id => $href) {
                            $menu = array("txt"=> $id, "href"=> $href);
                        }
                    }
                }
            } else {
                $resultados[] = array("success"=> false, "message"=> "No CFG File");
            }
            if ($lev == 1) {
                $resultados[] = array("success"=> true, "message"=> "SUDODMIN", "id"=> $rowp['id'], "level" => $lev, "nom"=> $rowp['nom'], "ape"=> $rowp['ape'], "apm"=> $rowp['apm'], "tel"=> $rowp['tel'], "cel"=> $rowp['cel'], "mai"=> $rowp['mai'], "ref"=> $rowp['ref'], "fec"=> $fec, "fea"=> $fea, "menu"=> $menu);
            } elseif ($lev == 2) {
                $resultados[] = array("success"=> true, "message"=> "Cliente", "id"=> $rowp['id'], "level" => $lev, "nom"=> $rowp['nom'], "ape"=> $rowp['ape'], "apm"=> $rowp['apm'], "tel"=> $rowp['tel'], "cel"=> $rowp['cel'], "mai"=> $rowp['mai'], "ref"=> $rowp['ref'], "fec"=> $fec, "fea"=> $fea, "menu"=> $menu);
            } elseif ($lev == 3) {
                $resultados[] = array("success"=> true, "message"=> "Vendedor", "id"=> $rowp['id'], "level" => $lev, "nom"=> $rowp['nom'], "ape"=> $rowp['ape'], "apm"=> $rowp['apm'], "tel"=> $rowp['tel'], "cel"=> $rowp['cel'], "mai"=> $rowp['mai'], "ref"=> $rowp['ref'], "fec"=> $fec, "fea"=> $fea, "menu"=> $menu);
            } elseif ($lev == 4) {
                $resultados[] = array("success"=> true, "message"=> "Administrador", "id"=> $rowp['id'], "level" => $lev, "nom"=> $rowp['nom'], "ape"=> $rowp['ape'], "apm"=> $rowp['apm'], "tel"=> $rowp['tel'], "cel"=> $rowp['cel'], "mai"=> $rowp['mai'], "ref"=> $rowp['ref'], "fec"=> $fec, "fea"=> $fea, "menu"=> $menu);
            }
        } else {
            $resultados[] = array("success"=> false, "message"=> "No se Encontro Nivel, Contacta Soporte");
        }
    } else {
        $resultados[] = array("success"=> false, "message"=> "No se inicio sesion");
    }

    print json_encode($resultados);
    include_once('../functions/cierra_conexion.php');
?>