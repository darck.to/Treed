<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    include_once('../functions/abre_conexion.php');

    $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
    $user = mysqli_real_escape_string($mysqli,$_POST['user']);

    $sql = $mysqli->query("SELECT id FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
    if ($sql->num_rows > 0) {
        $row = $sql->fetch_assoc();
        $sqlUsr = $mysqli->query("SELECT id FROM perf_table WHERE lev = 2");
        if ($sqlUsr->num_rows > 0) {
            while ($rowUsr = $sqlUsr->fetch_assoc()) {
                $sqlp = $mysqli->query("SELECT `perf_table`.`id_usr` as id, `perf_table`.`fec` as fea, `pag_table`.`fec` as fec, `pag_table`.`fla` as fla, `pag_table`.`ref` as rep, `perf_table`.`nom` as nom, `perf_table`.`ape` as ape, `perf_table`.`apm` as apm, `perf_table`.`cel` as cel, `perf_table`.`mai` as mai, `perf_table`.`ref` as ref, `perf_table`.`fat` as fat FROM `perf_table` LEFT JOIN `pag_table` ON `pag_table`.`id_usr` = `perf_table`.`id_usr` WHERE `perf_table`.`id` = '".$rowUsr['id']."'");
                if ($sqlp->num_rows > 0) {
                    $rowp = $sqlp->fetch_assoc();
                    // Fecha de pago
                    $fec = strtotime($rowp['fec']);
                    $fec = date("d/m/y g:i A", $fec);
                    $fec = date("d/m/y", strtotime($fec. ' + 1 years'));
                    // Fecha de afiliacion
                    $fea = strtotime($rowp['fea']);
                    $fea = date("d/m/y g:i A", $fea);
                    // Referencia father
                    $sqlFat = $mysqli->query("SELECT nom, ape, apm FROM perf_table WHERE ref = '".$rowp['fat']."'");
                    if ($sqlFat->num_rows > 0) {
                        $rowFat = $sqlFat->fetch_assoc();
                        $father = $rowFat['nom'] . " " . $rowFat['ape'] . " " . $rowFat['apm'];
                    } else {
                        $father = null;
                    }
                    $resultados[] = array("success"=> true, "message"=> "", "id" => $rowp['id'], "pago" => $rowp['fla'], "nom"=> $rowp['nom'], "ape"=> $rowp['ape'], "apm"=> $rowp['apm'], "cel"=> $rowp['cel'], "mai"=> $rowp['mai'], "ref"=> $rowp['ref'], "rep"=> $rowp['rep'], "fec"=> $fec, "fea"=> $fea, "fat"=> $father);
                }
            }
        } else {
            $resultados[] = array("success"=> false, "message"=> "No se Encontro Nivel, Contacta Soporte");
        }
    } else {
        $resultados[] = array("success"=> false, "message"=> "No se inicio sesion");
    }

    print json_encode($resultados);
    include_once('../functions/cierra_conexion.php');
?>