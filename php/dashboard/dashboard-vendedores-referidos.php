<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    include_once('../functions/abre_conexion.php');

    $ref = mysqli_real_escape_string($mysqli,$_POST['ref']);
    
    $sqlp = $mysqli->query("SELECT `perf_table`.`fec` as fea, `pag_table`.`fec` as fec, `perf_table`.`lev` as lev, `perf_table`.`nom` as nom, `perf_table`.`ape` as ape, `perf_table`.`apm` as apm, `perf_table`.`tel` as tel, `perf_table`.`cel` as cel, `perf_table`.`mai` as mai, `perf_table`.`ref` as ref FROM `perf_table` LEFT JOIN `pag_table` ON `pag_table`.`id_usr` = `perf_table`.`id_usr` WHERE `perf_table`.`fat` = '".$ref."'");
    if ($sqlp->num_rows > 0) {
        while ($rowp = $sqlp->fetch_assoc()) {
            $lev = $rowp['lev'];
            $fec = strtotime($rowp['fec']);
            $fec = date("d/m/y g:i A", $fec);
            $fec = date("d/m/y", strtotime($fec. ' + 1 years'));
            $fea = strtotime($rowp['fea']);
            $fea = date("d/m/y g:i A", $fea);
            $resultados[] = array("success"=> true, "message"=> "", "level" => $lev, "nom"=> $rowp['nom'], "ape"=> $rowp['ape'], "apm"=> $rowp['apm'], "tel"=> $rowp['tel'], "cel"=> $rowp['cel'], "mai"=> $rowp['mai'], "ref"=> $rowp['ref'], "fec"=> $fec, "fea"=> $fea);
        }
    } else {
        $resultados[] = array("success"=> false, "message"=> "No Referidos");
    }

    print json_encode($resultados);
    include_once('../functions/cierra_conexion.php');
?>