<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    include_once('../functions/abre_conexion.php');

    $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
    $user = mysqli_real_escape_string($mysqli,$_POST['user']);

    $sql = $mysqli->query("SELECT id FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
    if ($sql->num_rows > 0) {
        $row = $sql->fetch_assoc();
        $id = $row['id'];
        if ($sqlPerf = $mysqli->query("SELECT lev, ref, fat FROM perf_table WHERE id_usr = $id")) {
            if ($sqlPerf->num_rows > 0) {
                $rowP = $sqlPerf->fetch_assoc();
                if ($rowP['lev'] == 2) {
                    $referenciaUsar = $rowP['fat'];
                    $linkRegistro = false;
                } elseif ($rowP['lev'] == 3 || $rowP['lev'] == 1) {
                    $referenciaUsar = $rowP['ref'];
                    $linkRegistro = true;
                } else {
                    $referenciaUsar = $rowP['ref'];
                    $linkRegistro = false;
                }
                $resultados[] = array("success"=> true, "message"=> "Cargando Usuario", "ref"=> $referenciaUsar, "link" => $linkRegistro);
            }
        } else {
            $resultados[] = array("success"=> false, "message"=> "Error con tu perfil, contacta soporte " . mysqli_error($mysqli));
        }
    } else {
        $resultados[] = array("success"=> false, "message"=> "No se inicio sesion");
    }

    print json_encode($resultados);
    include_once('../functions/cierra_conexion.php');
?>
