<?php
    header("Access-Control-Allow-Origin: *");
    header('Content-type: application/json');
    include_once('../functions/abre_conexion.php');

    $auth = mysqli_real_escape_string($mysqli,$_POST['auth']);
    $user = mysqli_real_escape_string($mysqli,$_POST['user']);
    
    $sql = $mysqli->query("SELECT id FROM auth_table WHERE init_index = '".$auth."' AND nom = '".$user."' ");
    if ($sql->num_rows > 0) {
        $row = $sql->fetch_assoc();
        // Si es licencia de id
        if (isset($_POST['id'])) {
            $id = mysqli_real_escape_string($mysqli,$_POST['id']);
            $consulta = "SELECT fla, fec, ref FROM pag_table WHERE id_usr = '".$id."'";
            $consulta2 = "SELECT usr, pas, srv, mon, rik FROM met_table WHERE id_usr = '".$id."'";
        } else {
            $consulta = "SELECT fla, fec, ref FROM pag_table WHERE id_usr = '".$row['id']."'";
            $consulta2 = "SELECT usr, pas, srv, mon, rik FROM met_table WHERE id_usr = '".$row['id']."'";
        }
        $sqlp = $mysqli->query($consulta);
        if ($sqlp->num_rows > 0) {
            $rowp = $sqlp->fetch_assoc();
            $fla = $rowp['fla'];
            $ref = $rowp['ref'];
            $sqMet = $mysqli->query($consulta2);
            if ($sqMet->num_rows > 0) {
                $rowM = $sqMet->fetch_assoc();
                if ($fla == 1) {
                    $resultados[] = array("success"=> true, "message"=> "Licencia Activa", "usr"=> $rowM['usr'], "pas"=> $rowM['pas'], "srv"=> $rowM['srv'], "mon"=> $rowM['mon'], "rik"=> $rowM['rik'], "ref"=> $ref);
                } elseif ($fla == 0) {
                    $resultados[] = array("success"=> false, "message"=> "Licencia Vencida", "usr"=> $rowM['usr'], "pas"=> $rowM['pas'], "srv"=> $rowM['srv'], "mon"=> $rowM['mon'], "rik"=> $rowM['rik'], "ref"=> $ref);
                }
            }
        } else {
            $resultados[] = array("success"=> false, "message"=> "No se Encontro Licencia");
        }
    } else {
        $resultados[] = array("success"=> false, "message"=> "No se inicio sesion");
    }

    print json_encode($resultados);
    include_once('../functions/cierra_conexion.php');
?>
