<?php
	//GENERADOR DE CADENAS ALEATORIAS
	function generateRandomString($length) {
	    $characters = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZasdfgqwertzxcvbpoiuylkjhmn';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
	// Referencias numericas only
	function generateRandomRef($length) {
	    $characters = '1234567890QDRYGHFGVXBBMFPROWIUWJSCXZZXCVNALWO00';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
	//CAMBIAMOS PESO DE LAS IMAGENES
	function compress($source, $destination, $quality) {
		$info = getimagesize($source);
		if ($info['mime'] == 'image/jpeg') 
		  $image = imagecreatefromjpeg($source);
		elseif ($info['mime'] == 'image/gif') 
		  $image = imagecreatefromgif($source);
		elseif ($info['mime'] == 'image/png') 
		  $image = imagecreatefrompng($source);
		imagejpeg($image, $destination, $quality);
		return $destination;
	}
	//FUNCTION COMPRUEBA EMAIL
  	function validaEmail($correo) {
	  if (preg_match('/^[A-Za-z0-9-_.+%]+@[A-Za-z0-9-.]+\.[A-Za-z]{2,4}$/', $correo)) return true;
	    else return false;
	}
?>