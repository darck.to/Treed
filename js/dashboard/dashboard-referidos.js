(function() {
  // Title 1 & 2
  $('#title-1').html('Referidos');
  $('#title-2').html('Visualización y revisión de datos');
  
  // Comprobamos si existe la session
  var a = localStorage.getItem("feed_key");
  var u = localStorage.getItem("feed_user")
  
  cargaReferidos(a,u);
}());

// Carga Vendedor
function cargaReferidos(a,u) {
    $.ajax({
      type: 'POST',
      url: 'php/dashboard/dashboard-referidos.php',
      data: {
        auth : a,
        user : u
      },
      async: true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        let txt = '<div class="box table-container has-background-white">';
        txt += '<table class="table is-fullwidth" id="datatable">';
        txt += '<thead>';
          txt += '<tr>';
            txt += '<th>Cliente</th>';
            txt += '<th>Tel&eacute;no</th>';
            txt += '<th>Celular</th>';
            txt += '<th>Email</th>';
            txt += '<th>Caducidad</th>';
            txt += '<th>Inscripci&oacute;n</th>';
            txt += '<th>Accciones</th>';
          txt += '</tr>';
        txt += '</thead>'
        $.each(data, function (name, value) {
          if (value.success == true) {
            txt += '<tr class="is-size-7 trows">';
              txt += '<td>' + value.nom + ' ' + value.ape + ' ' + value.apm + '</td>';
              txt += '<td><a href="tel:' + value.tel + '">' + value.tel + '</a></td>';
              txt += '<td><a href="tel:' + value.cel + '">' + value.cel + '</a></td>';
              txt += '<td><a href="mailto:' + value.mai + '">' + value.mai + '</a></td>';
              txt += '<td>' + value.fec + '</td>';
              txt += '<td>' + value.fea + '</td>';
              txt += '<td class="has-text-centered"><a href="https://api.whatsapp.com/send?phone=52' + value.cel + '" target="_blank"><i class="fa-brands fa-2x has-text-success fa-whatsapp"></i></a></td>';
            txt += '</tr>'
          } else {
            toast(value.message)
          }
        })
        txt += '</table>';
        txt += '</div>';
        $('#infoTable').html(txt);
        // Tables plugin
        $('#datatable').DataTable({
            "dom": 'Blfrtip',
            "bProcessing": true,
            buttons: [
                ''
            ]
        });
        // Selecciona rows
        $('.table .trows').on('click', function(e) {
          $('.table tr').removeClass('is-selected');
          $(this).toggleClass('is-selected')
        })
      },
      error: function(xhr, tst, err) {
        toast('No se pudo revisar la licencia, contacta soporte')
      }
    })
  }