(function() {
  // Title 1 & 2
  $('#title-1').html('Clientes');
  $('#title-2').html('Visualización, activación de cuentas y revisión de datos');
  
  // Comprobamos si existe la session
  var a = localStorage.getItem("feed_key");
  var u = localStorage.getItem("feed_user")
  
  cargaClientes(a,u);
}());

// Carga Vendedor
function cargaClientes(a,u) {
  $.ajax({
    type: 'POST',
    url: 'php/dashboard/dashboard-clientes-carga.php',
    data: {
      auth : a,
      user : u
    },
    async: true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      let txt = '<div class="box table-container has-background-white">';
      txt += '<table class="table is-fullwidth is-relative" id="datatable">';
      txt += '<thead>';
        txt += '<tr>';
          txt += '<th>Pago</th>';
          txt += '<th>Cliente</th>';
          txt += '<th>Celular</th>';
          txt += '<th>Email</th>';
          txt += '<th>Caducidad</th>';
          txt += '<th>Inscripci&oacute;n</th>';
          txt += '<th>Accciones</th>';
        txt += '</tr>';
      txt += '</thead>'
      $.each(data, function (name, value) {
        if (value.success == true) {
          // Flag de pago
          txt += '<tr class="is-size-7 trows no-cmenu" data-id="' + value.id + '" data-rep="' + value.rep + '" data-flag="' + value.pago + '">';
            txt += '<td class="has-text-centered">' + pay + '</td>';
            txt += '<td>' + value.nom + ' ' + value.ape + ' ' + value.apm + '</td>';
            txt += '<td><a href="tel:' + value.cel + '">' + value.cel + '</a></td>';
            txt += '<td><a href="mailto:' + value.mai + '">' + value.mai + '</a></td>';
            txt += '<td>' + value.fec + '</td>';
            txt += '<td>' + value.fea + '</td>';
            txt += '<td class="has-text-centered"><a href="https://api.whatsapp.com/send?phone=52' + value.cel + '" target="_blank"><i class="fa-brands fa-2x has-text-success fa-whatsapp"></i></a></td>';
          txt += '</tr>'
        } else {
          toast(value.message)
        }
      })
      txt += '</table>';
      txt += '</div>';
      $('#infoTable').html(txt);
      // Tables plugin
      $('#datatable').DataTable({
          "dom": 'Blfrtip',
          "bProcessing": true,
          buttons: [
              'excelHtml5'
          ]
      });
      // Selecciona rows
      $('.table .trows').on('click', function(e) {
        $('.table tr').removeClass('is-selected');
        $(this).toggleClass('is-selected')
      })
      // Menu contextual
      $('.no-cmenu').bind("contextmenu",function(e) {
        let top = e.pageY-400;
        let left = e.pageX-350;
        // Html menu
        let txt = '<div class="box context-menu has-background-white-ter">';
          txt += '<ul>';
            txt += '<li class="is-clickable context-action">Referencia de Pago: ' + $(this).data('rep') + '</li>';
            txt += '<li class="is-clickable context-action" data-edit="0" data-id="' + $(this).data('id') + '">Editar</li>';
            txt += '<li class="is-clickable context-action" data-meta="0" data-id="' + $(this).data('id') + '">Metatrader</li>';
            // Activacion de cuenta
            let pay, link;
            $(this).data('flag') == "0" ? pay = 'Activar Cuenta' : pay = 'Desactivar';
            $(this).data('flag') == "0" ? link = 'context-action' : link = 'has-text-grey-light';
            txt += '<li class="is-clickable ' + link + '" data-activate="0" data-id="' + $(this).data('id') + '">' + pay + '</li>';
            txt += '<hr class="p-0 m-0">';
            txt += '<li class="is-clickable has-text-danger">Borrar</li>';
          txt += '</ul>';
        txt += '</div>';
        $(this).append(txt)
        // Show contextmenu
        $('.context-menu').toggle(100).css({
            top: top + "px",
            left: left + "px"
        })
        // Acciones de click del menu contextual
        $('.context-action').on('click', function(e) {
          // Si es meta datos
          if ($(this).data('meta') == 0) {
            let id = $(this).data('id');
            revisaLicencia(a,u,id)
            // Si es edicion
          } else if($(this).data('edit') == 0) {
            edicionUsuario($(this).data('id'),'edit')
            // Si es activacion
          } else if($(this).data('activate') == 0) {
            edicionUsuario($(this).data('id'),'activate')
          }
        });
        return false
      })
      // Borra el contextmenu
      $(document).mouseup(function(e) {
        var container = $('.context-menu')
        if (!container.is(e.target) && container.has(e.target).length === 0) {
          container.remove()
        }
      });
        // disable context-menu from custom menu
      $('.context-menu').bind('contextmenu',function(){
        return false
      });
      // Clicked context-menu item
      $('.context-menu li').click(function(){
        toast('helo')
      })
    },
    error: function(xhr, tst, err) {
      toast('No se pudo revisar la licencia, contacta soporte')
    }
  })
}

// Revisa licencia  
function revisaLicencia(a,u,i) {
  $.ajax({
    type: 'POST',
    url: 'php/dashboard/dashboard-comprueba-licencia.php',
    data: {
      auth : a,
      user : u,
      id : i
    },
    async: true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $.each(data, function (name, value) {
        if (value.success == true) {
          toast(value.message);
          procesoLicencia(a,u,i,value.usr, value.pas, value.srv, value.mon, value.rik, value.ref)
        } else {
          toast(value.message);
          procesoLicencia(a,u,i,value.usr, value.pas, value.srv, value.mon, value.rik, value.ref)
        }
      })
    },
    error: function(xhr, tst, err) {
      toast('No se pudo revisar la licencia, contacta soporte');
      modax()
    }
  })
}

// Edita informacion
function edicionUsuario(i,n) {
  let id = i;
  let name = n;
  switch (name) { 
    case 'edit': 
      modalEdita()
      break;
    case 'activate': 
      modalEdita()
      break;
    default:
      toast('Error')
  }
  // Edit modal
  function modalEdita() {
    $.get('templates/init/init-signin.html', function (data) {
      modal(data);
      // Personalizamos
      $('.reg-title').remove();
      $('.buttonLoginForm').remove();
      //$('#r-fat').val(value.ref);
      $('#r-fat').css('pointer-events','none');
      $('#r-fat').css('background','hsl(0, 0%, 86%)');
      //$('.signup-section form').removeAttr('id');
      $('.siteFlag').val('off');
      $('.signButton').html('Editar');
      // Reaparece
      $('.card-register').removeClass('is-hidden')
      // EditaButton
      $('.signButton').on('click', function(e) {
        $('.signButton').addClass('is-loading');
        //if ($('.siteFlag').val() == 'off') { return false }
        e.preventDefault()

        var formData = new FormData();
        formData.append('auth',a);
        formData.append('user', u);
        formData.append('nom', $('#r-nom').val());
        formData.append('ape', $('#r-ape').val());
        formData.append('apm', $('#r-apm').val());
        formData.append('usr', $('#r-usr').val());
        formData.append('mai', $('#r-mai').val());
        formData.append('cel', $('#r-cel').val());
        formData.append('pas', $('#r-pas').val());
        formData.append('fat', $('#r-fat').val());

        var request = $.ajax({
          url: 'php/init/init-register.php',
          method: 'POST',
          data: formData,
          contentType: false,
          processData: false,
          async: true,
          dataType : 'json',
          crossDomain: true,
          context: document.body,
          cache: false,
        });
        // handle the responses
        request.done(function(data) {
          $.each(data, function (name, value) {
          if (value.success) {
          toast(value.message);
          $('.signButton').removeClass('is-loading');
          $('#initRegisterUser').trigger('reset')
          } else {
          $('.signButton').removeClass('is-loading');
          toast(value.error)
          }
          })
        })
        request.fail(function(jqXHR, textStatus) {
          $('.signButton').removeClass('is-loading');
          console.log(textStatus);
        })
        request.always(function(data) {
          // clear the form
          //$('#initRegisterUser').trigger('reset');
        })
      })
    })
  }
  
  if (e.which == 13) {
    $.ajax({
      type: 'POST',
      url: 'php/dashboard/dashboard-cliente-edicion.php',
      data: {
        auth : a,
        user : u,
        id : i,
        name : name,
        value : value
      },
      async: true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        $.each(data, function (name, value) {
          if (value.success == true) {
            toast(value.message)
          } else {
            toast(value.message)
          }
        })
      },
      error: function(xhr, tst, err) {
        toast('No se pudo revisar la licencia, contacta soporte')
      }
    })
  }
}