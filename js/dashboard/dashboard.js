(function() {
  // Comprobamos si existe la session
  var stringafeed_key = localStorage.getItem("feed_key");
  var stringafeed_user = localStorage.getItem("feed_user");
  var loginToken = localStorage.getItem("feed_token");
  
  comprueba_login(stringafeed_key,stringafeed_user,loginToken);
}());

function comprueba_login(a,u,t) {
  //COMPROBAMOS SI EXISTE LOGIN LOCAL
  if (t == 0 || t == null) {
    //LOCAL LOGIN NULL OR NEGATIVE
    $.ajax({
      type: 'POST',
      url: 'php/init/init-comprueba-auth.php',
      data: {
        auth : a,
        user : u
      },
      async: true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        var aprobacion
        $.each(data, function (name, value) {
          if (value.success == true) {
            localStorage.setItem("feed_token",1);
            localStorage.setItem("feed_type",value.type);
            toast(value.message)
            //LOGIN LOCAL POSITIVE
            cargaDashboard(a,u)
          } else {
            // Login
            window.location.href = 'index.html'
          }
        })
      },
      error: function(xhr, tst, err) {
        toast('El tiempo de espera fue superado, por favor intentalo en un momento mas');
        modax()
      }
    })
  } else {
    //LOGIN LOCAL POSITIVE
    cargaDashboard(a,u)
  }
}

function cargaDashboard(a,u) {
  $('#dashboardTemplate').load('templates/dashboard/dashboard-template.html', function (e) {
    // Check for click events on the navbar burger icon
    $(".navbar-burger").click(function() {
      $(".navbar-burger").toggleClass("is-active");
      $(".navbar-menu").toggleClass("is-active")
    })
    // Login out
    $('.buttonLogout').on('click', function(e) {
      localStorage.removeItem("feed_key");
      localStorage.removeItem("feed_user");
      localStorage.removeItem("feed_token");
      modal('<h5 class="p-4 has-text-right">cerrando sesi&oacute;n...&nbsp;<i class="fa-solid fa-person-walking-arrow-right"></i></h5>');
      setTimeout(location.reload.bind(location), 1000);
    })
    // Revisa el nivel de usuario
    revisaLevel(a,u);
  })
}

function revisaLevel(a,u) {
  $.ajax({
    type: 'POST',
    url: 'php/dashboard/dashboard-comprueba-level.php',
    data: {
      auth : a,
      user : u
    },
    async: true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      let id;
      $.each(data, function (name, value) {
        if (value.success == true) {
          // Menu append
          $.each(value.menu, function (id, content) {
            $.each(content, function (txt, href) {  
              $('#menuLevel').append('<li><a data-target="' + href + '">' + txt + '</a></li>')
            })
          })
          // Cargamos los datos en dashboard
          id = value.id;
          $('#usrName').html(value.nom + ' ' + value.ape + ' ' + value.apm);
          $('#usrContact').html('<p>Tel: ' + value.tel + '</p><p>Cel: ' + value.cel + '</p><p>Email: ' + value.mai + '</p>');
          // Fecha de registro
          $('#usrClientDate').html(value.fea);
          // Fecha de caducidad de licencia
          $('#usrLicDate').html(value.fec);
          toast(value.message)
          if (value.level == 1) {
            //Super admin
            toast('Perfil SU');
            cargaPrimerMenu();
            revisaLicencia(a,u,id)
          } else if (value.level == 2) {
            // Cliente
            // Revisa la vigencia de la licencia
            revisaLicencia(a,u,id)
          } else if (value.level == 3) {
            // Vendedor
            toast('Perfil Vendedor');
            cargaPrimerMenu()
          } else if (value.level == 4) {
            // Administrativo
            toast('Perfil Administrador');
            cargaPrimerMenu()
          }
        } else {
          toast(value.message);
          procesoLicencia(a,u)
        }
      })
      $('.menu a').on('click', function (e) {
        $('.menu a').removeClass('is-active');
        $(this).toggleClass('is-active')
      })
      // Cargamos las secciones del menu en body
      $('.menu a').on('click', function (e) {
        e.preventDefault();
        // Breadcrumb
        $('#breadCurrent').html($(this).data('target'));
        $('#dashboard-body').html('');
        $('#dashboard-body').load('templates/dashboard/dashboard-' + $(this).data('target') + '.html')
      })
    },
    error: function(xhr, tst, err) {
      toast('No se pudo revisar la licencia, contacta soporte');
      modax()
    }
  })
}

function cargaPrimerMenu(e) {
  $('#breadCurrent').html('Principal');
  $('#dashboard-body').html('');
  $('#dashboard-body').load('templates/dashboard/dashboard-dashboard.html')
}

function revisaLicencia(a,u,i) {
  $.ajax({
    type: 'POST',
    url: 'php/dashboard/dashboard-comprueba-licencia.php',
    data: {
      auth : a,
      user : u
    },
    async: true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      $.each(data, function (name, value) {
        if (value.success == true) {
          toast(value.message)
        } else {
          toast(value.message);
          procesoLicencia(a,u,i,value.usr, value.pas, value.srv, value.mon, value.rik, value.ref)
        }
      })
    },
    error: function(xhr, tst, err) {
      toast('No se pudo revisar la licencia, contacta soporte');
      modax()
    }
  })
}

(function() {

  window.procesoLicencia = function(a,u,i,usr,pas,srv,mon,rik,ref) {
    let x = '<div class="card-header">';
      x += '<p class="card-header-title">Activación de Licencia</p>';
    x += '</div>'
    x += '<div class="tabs is-small is-toggle is-fullwidth">';
      x += '<ul>';
        x += '<li class="tabNav is-active" data-tab="0"><a>Reg&iacute;strate en FBS</a></li>';
        x += '<li class="tabNav" data-tab="1"><a>Ingresa tus datos de METATRADER</a></li>';
        x += '<li class="tabNav" data-tab="2"><a>Financ&iacute;a tu Cuenta</a></li>'
      x += '</ul>';
    x += '</div>';
    // Paso 1 registrarse en BROKER -- Pasos a cumplir
    x += '<div class="card-content navCard nav-0 firstScreen">';
      x += '<div class="columns is-multiline">';
        x += '<div class="column">';
          x += '<h3>INSTRUCCIONES:</h3>';
          x += '<ul class="m-3">';
            x += '<li class="p-1">Da click en Registrate Aqu&iacute;&nbsp;<i class="fa-solid fa-person-walking-arrow-right"></i></li>';
            x += '<li class="p-1">Introduce tu correo y nombre completo</li>';
            x += '<li class="p-1">Selecciona cuenta <b>CENT</b></li>';
            x += '<li class="p-1">Seleccionar <b>METATRADER 4, apalancamiento 1:1000 y USD</b></li>';
          x += '</ul>';
          x += '<div class="notification is-warning">En el siguiente paso es muy importante guardar la informaci&oacute;n de registro para poder seguir llenando el siguiente <b>formato</b> (Usuario, Contraseña y Servidor de METATRADER)</div>';
          x += '<hr class="m-6">';
          x += '<h2 class="mb-3">Registrate desde el botón (As&iacute; sabremos que eres tu)</h2>';
          x += '<a class="button mb-3 is-link w-100 has-text-weight-light" href="https://fbs.partners/?ibl=62179&ibp=12683006" target="_blank">Registrate Aqu&iacute;&nbsp;<i class="fa-solid fa-person-walking-arrow-right"></i></a>';
            x += '<hr class="m-6">';
            x += '<p><b>*</b>Te recomendamos depositar los fondos por medio de la opci&oacute;n <b>VISA-MASTERCARD (tarjeta de cr&eacute;dito o d&eacute;bito) ya que no genera comisi&oacute;n</b>. (Si tu primer dep&oacute;sito es denegado, tendr&aacute;s que llamar a tu banco para autorizar la transacci&oacute;n e intentar de nuevo, si a&uacute;n as&iacute; dep&oacute;sito es denegado, te recomendamos hacerlo por medio de SPEI)';
        x += '</div>';
      x += '</div>';
    x += '</div>';
    // Paso 2 datos de servidor METATRADER -- Revisamos y cargamos las licencias del cliente
    x += '<div class="card-content is-hidden navCard nav-1 secondScreen">';
      x += '<div class="columns is-multiline">';
        x += '<div class="column is-6">';
          x += '<div class="field">';
            x += '<div class="control tooltip" data-tooltip="Presiona ENTER para guardar">';
              x += '<label class="label has-text-weight-light">Nombre de usuario en Metatrader</label>';
              x += '<input class="input is-small inputMeta inpUsr" name="usr" type="text" placeholder="Usuario de Metatrader" required value="' + usr + '">';
            x += '</div>';
          x += '</div>';
        x += '</div>';
        x += '<div class="column is-6">';
          x += '<div class="field">';
            x += '<div class="control tooltip" data-tooltip="Presiona ENTER para guardar">';
              x += '<label class="label has-text-weight-light">Contraseña de Metatrader</label>';
              x += '<input class="input is-small inputMeta inpKey" name="pas" type="text" placeholder="Contraseña" required value="' + pas + '">';
            x += '</div>';
          x += '</div>';
        x += '</div>';
        x += '<div class="column is-6">';
          x += '<div class="field">';
            x += '<div class="control tooltip" data-tooltip="Presiona ENTER para guardar">';
              x += '<label class="label has-text-weight-light">Servidor de Metatrader (ej. FBS-Demo)</label>';
              x += '<input class="input is-small inputMeta inpSrv" name="srv" type="text" placeholder="Servidor de Metatrader" required value="' + srv + '">';
            x += '</div>';
          x += '</div>';
        x += '</div>';
        x += '<div class="column is-6">';
          x += '<div class="field">';
            x += '<div class="control tooltip" data-tooltip="Presiona ENTER para guardar">';
              x += '<label class="label has-text-weight-light">Cantidad a Invertir</label>';
              x += '<input class="input is-small inputMeta inpMnt" name="mon" type="text" placeholder="Monto de Inversión" required value="' + mon + '">';
            x += '</div>';
          x += '</div>';
        x += '</div>';
        x += '<div class="column is-12">';
        x += '<label class="label has-text-weight-light">Nivel de la Cuenta</label>';
          x += '<div class="columns">';
              x += '<div class="column is-4">';
                x += '<div class="control">';
                  x += '<label class="radio">';
                    // Nivel de riesgo RIK
                    let oneRadio = '', twoRadio = '', threeRadio = '';
                    if (rik == 1) {
                      oneRadio = "checked"
                    } else if (rik == 2) {
                      twoRadio = "checked"
                    } else if (rik == 3) {
                      threeRadio = "checked"
                    }
                    x += '<input class="mr-1" type="radio" name="rik" value="1" ' + oneRadio + '>';
                    x += 'Alto&nbsp;<span class="is-size-7">Rendimiento aproximado mensual 10%, cierre de operaciones al 60%)</span>';
                  x += '</label>';
                x += '</div>';
              x += '</div>';
              x += '<div class="column is-4">';
                x += '<div class="control">';
                  x += '<label class="radio">';
                    x += '<input class="mr-1" type="radio" name="rik" value="2" ' + twoRadio + '>';
                    x += 'Medio&nbsp;<span class="is-size-7">Rendimiento aproximado mensual 5%, cierre de operaciones al 45%)</span>';
                  x += '</label>';
                x += '</div>';
              x += '</div>';
              x += '<div class="column is-4">';
                x += '<div class="control">';
                  x += '<label class="radio">';
                    x += '<input class="mr-1" type="radio" name="rik" value="3" ' + threeRadio + '>';
                    x += 'Bajo&nbsp;<span class="is-size-7">Rendimiento aproximado mensual 2.5%, cierre de operaciones al 30%)</span>';
                  x += '</label>';
                x += '</div>';
              x += '</div>';
            x += '</div>';
          x += '</div>';
        x += '</div>';
      x += '<hr class="m-6">';
      x += '<p class="is-size-6 mb-3 mt-4 has-text-weight-light">Es muy importante que estos datos sean correctos, de no ser así el proceso de registro de licencia puede demorar m&aacute;s de lo debido</p>';
      x += '<p class="is-size-7 mb-3 mt-4 has-text-weight-light">Ser&aacute;s redirigido al sitio de pago seguro donde podremos verificarlo una ves terminado el proceso, recuerda utilizar el mismo correo que registraste aqu&iacute;</p>';
      x += '<div class="columns pa-one">';
      x += '<div class="column is-6">';
          x += '<div class="field">';
            x += '<div class="control has-text-centered">';
              x += '<a class="has-text-weight-light button is-link w-100 buttonTicket" data-ref="' + ref + '">';
                x += '<span>Paga en Caja</span>';
              x += '</a>';  
            x += '</div>';
          x += '</div>';
        x += '</div>';
        x += '<div class="column is-6">';
          x += '<div class="field">';
            x += '<div class="control has-text-centered">';
              x += '<a href="https://buy.stripe.com/3cseZ0eNU86V6f66oo" class="has-text-weight-light button is-info w-100" target="_blank">';
                x += '<span>Pago en Linea&nbsp;<i class="fa-brands fa-stripe"></i></span>';
              x += '</a>';  
            x += '</div>';
          x += '</div>';
        x += '</div>';
      x += '</div>';
    x += '</div>';
    // Paso 3 deposita a tu cuenta de BROKER -- Finalizacion de pasos
    x += '<div class="card-content is-hidden navCard nav-2 thirdScreen">';
      x += '<div class="columns is-multiline">';
        x += '<div class="column">';
          x += '<h3>En tu cuenta de <b>FBS</b>:</h3>';
          x += '<p class="m-3">Ingresa al &Aacute;rea Personal de tu cuenta de FBS para validar tu identidad, tendr&aacute;s que adjuntar una copia de tu pasaporte (NO PODR&Aacute;S DEPOSITAR FONDOS A TU CUENTA HASTA VALIDAR TU IDENTIDAD)</p>';
          x += '<hr class="m-6">';
          x += '<div class="notification is-info">Una vez validada tu identidad, recuerda <strong>DEPOSITAR UN M&Iacute;NIMO DE $2,000 DLLS</strong> a tu cuenta de FBS</div>';
        x += '</div>';
      x += '</div>';
    x += '</div>';
    x += '</div>'
    modal(x)
    // TabNav
    $('.tabNav').on('click', function (e) {
      // Navigation
      let destiny = $(this).data('tab');
      $('.navCard').addClass('is-hidden');
      $('.nav-' + destiny).removeClass('is-hidden')
      // tab Styles
      $('.tabNav').removeClass('is-active');
      $(this).toggleClass('is-active')
    })
    // Guarda las llaves al dejar el input
    $('.inputMeta').on('focus', function(e) {
      let txt = $(this).parent('.tooltip').data('tooltip');
      let tool = '<span class="tooltiptext is-size-7 p-3">' + txt + '</span>';
      $(this).parent().append(tool)
    })// Lo elimina cuando sale
    $('.inputMeta').on('focusout', function(e) {
      $('.tooltiptext').remove()
    })// Se guarda con ENTER key
    $('.inputMeta').on('keypress', function(e) {
      let value = $(this).val();
      let name = $(this).attr("name");
      if (e.which == 13) {
        $.ajax({
          type: 'POST',
          url: 'php/dashboard/dashboard-activacion-datos.php',
          data: {
            auth : a,
            user : u,
            value : value,
            name : name,
            id : i
          },
          async: true,
          dataType : 'json',
          crossDomain: true,
          context: document.body,
          cache: false,
          success: function(data) {
            $.each(data, function (name, value) {
              if (value.success == true) {
                toast(value.message)
              } else {
                toast(value.message)
              }
            })
          },
          error: function(xhr, tst, err) {
            toast('No se pudo revisar la licencia, contacta soporte')
          }
        })
      }
    })
    // Guarda los input radio
    $('input:radio[name="rik"]').change(function() {
      let value = $(this).val();
      let name = $(this).attr("name")
      $.ajax({
        type: 'POST',
        url: 'php/dashboard/dashboard-activacion-datos.php',
        data: {
          auth : a,
          user : u,
          value : value,
          name : name,
          id : i
        },
        async: true,
        dataType : 'json',
        crossDomain: true,
        context: document.body,
        cache: false,
        success: function(data) {
          $.each(data, function (name, value) {
            if (value.success == true) {
              toast(value.message)
            } else {
              toast(value.message)
            }
          })
        },
        error: function(xhr, tst, err) {
          toast('No se pudo revisar la licencia, contacta soporte')
        }
      })
    });

    // Imprime el ticket de pago en caga
    $('.buttonTicket').on('click', function(event) {
      event.preventDefault()
      $(this).toggleClass('is-loading');
      let ref = $(this).data('ref');
      let txt;
      $.ajax({
        url: 'https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF43718/datos/oportuno?token=467d3de63a525dd6a822c5dbeb2194e2b45ed13890957bfab472326db02c9b5e',
        async: true,
        dataType : 'json',
          crossDomain: true,
          context: document.body,
          cache: false,
          success: function(data) {
            $.each(data, function (indexInArray, bmx) {
              $.each(bmx, function (index, series) { 
                $.each(series, function (id, value) { 
                  txt = '<div class="payCard">';
                    txt += '<div class="card-content">';
                      txt += '<div id="copypaste">';
                        txt += '<h1 class="is-title has-text-weight-light">Treed Inteligencia Financiera</h1>';
                        txt += '<span class="is-subtitle">Referencia de Pago: <b class="is-size-4">'  + ref + '</b></span>';
                        txt += '<p class="is-subtitle">Banco: BANORTE</p>';
                        txt += '<p class="is-subtitle">CLABE: 072930003307470153</p>';
                        txt += '<p class="is-subtitle">Cuenta: 0330747015</p>';
                        txt += '<span class="is-subtitle">Titular: M&oacute;nica Mart&iacute;nez Duarte</span>';
                        txt += '<p class="is-size-5 has-text-weight-light">Cantidad a Pagar: <span class="is-size-3">$(USD) 1000.00</span></p>';
                        txt += '<p class="is-size-5 has-text-weight-light">Cantidad a Pagar: <span class="is-size-3">$(MXN) ' + (1000 * value.datos[0].dato) + '</span></p>';
                        txt += '<p class="is-size-7 mb-3">' + value.titulo + ', ' + value.datos[0].fecha + ': TC: ' + value.datos[0].dato + ' <small><a href="https://www.banxico.org.mx/tipcamb/main.do?page=tip&idioma=sp" target="_blank">BANXICO</a></small></p>';
                      txt += '</div>';
                    txt += '</div>';
                    txt += '<footer class="card-footer">';
                      txt += '<a class="card-footer-item returnCard">Regresar</a>';
                      txt += '<a class="card-footer-item"><i class="fa-solid fa-copy has-text-link fa-2x is-clickable copyCard" alt="Copia al Portapales" data-object="copypaste"></i></a>';
                    txt += '</footer>';
                  txt += '</div>'
                  $('.secondScreen').toggleClass('is-hidden');
                  $('.modal-content .card').append(txt)
                })
              })
            })
            // Copy payCard          
            $('.copyCard').on('click', function(e){
              if($(this).data('object')){
                selectText($(this).data('object'));
                var link = $(this).text();
                var copied = document.execCommand("copy", link);
                if (copied) {
                  toast('Copiado Correctamente')
                }
              }
            })
            function selectText(containerid) {
              if (document.selection) {
                var range = document.body.createTextRange();
                range.moveToElementText(document.getElementById(containerid));
                range.select();
              } else if (window.getSelection) {
                var range = document.createRange();
                range.selectNode(document.getElementById(containerid));
                window.getSelection().addRange(range);
              }
            }
            // Sale del Paycard
            $('.returnCard').on('click', function(e) {
              $('.buttonTicket').toggleClass('is-loading');
              $('.secondScreen').toggleClass('is-hidden');
              $('.payCard').remove()
            })

          },
          error: function(xhr, tst, err) {
            toast('Tipo de cambio no disponible')
          }
      })
    })

  } // Se acabo el proceso de Licencia

}());