$(function() {
    // Menu
    $('#menu').load('templates/menu/menu.html', function(e) {

        // Template
        $('#principalTemplate').load('templates/principal/principal.html', function(e) {

            // Input test update chart
            $('#cuantInput').keypress(function(event) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == '13' && $.isNumeric($(this).val())) {
                    calculaTable($(this).val())
                }
            }) // Boton de calculo
            $('#calcInput').on('click', function(event) {
                if ($.isNumeric($('#cuantInput').val())) {
                    calculaTable($(this).val())
                }
            })
            // Crea la table
            function calculaTable(n) {
                let opcion = $('input[name="typeInput"]:checked').val();
                let i = 0, inversion = $('#cuantInput').val();
                let txt = '<div class="box" style="overflow-x:auto;">'
                    txt += '<h2>Resultados</h2>';
                    txt += '<table class="table is-striped is-hoverable is-fullwidth">';
                    let meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
                    let capital = [], utilidades = [];
                    txt += '<tr>';
                        txt += '<th><small>Periodo</small></th>';
                    $.each(meses, function (id, value) {
                        txt += '<th>' + value + '</th>'
                    })
                    txt += '</tr>'
                    while (i < 12) {
                        capital.push(Math.round(inversion));
                        utilidades.push(Math.round((inversion)*opcion));
                        inversion = inversion * opcion;
                        // Table data
                        //myChart.data.datasets[0].data[i] = Math.round(inversion);
                        //myChart.data.datasets[1].data[i] = (Math.round(inversion)*opcion);
                        i++
                    }
                // Add a Vertical Table
                txt = '<div class="box" style="overflow-x:auto;">'
                    txt += '<h2 class="is-title">Resultados</h2>';
                    txt += '<table class="table is-striped is-hoverable is-fullwidth">';
                        txt += '<tr>';
                            txt += '<th><small>Periodo</small></th>';
                            txt += '<th class="has-text-centered">Balance</th>';
                            txt += '<th class="has-text-centered">Balance Final</th>';
                            txt += '<th class="has-text-centered">Ganancias</th>';
                            txt += '<th class="has-text-centered">Ganancias Total %</th>';
                        txt += '</tr>'
                let ganancia = 0, porcentaje = 0;
                for (let index = 0; index < meses.length; index++) {
                    ganancia = ganancia+utilidades[index]-capital[index];
                    porcentaje = Math.round((100*ganancia)/capital[0]);
                    txt += '<tr><td class="has-text-weight-bold">' + meses[index] + '</td><td class="has-text-centered">' + milesNumber(capital[index]) + '</td><td class="has-text-centered">' + milesNumber(utilidades[index]) + '</td><td class="has-text-centered">' + milesNumber(ganancia) + '</td><td class="has-text-centered">' + porcentaje + '%</td></tr>'
                }
                $('#chartTable').html(txt);
                //myChart.update()
            }
            // Revisar si es miles
            function milesNumber(n) {
                if (typeof n === 'number') {
                    n += '';
                    var x = n.split('.');
                    var x1 = x[0];
                    var x2 = x.length > 1 ? '.' + x[1] : '';
                    var rgx = /(\d+)(\d{3})/;
                    while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + ',' + '$2');
                    }
                    return x1 + x2;
                } else {
                    return n;
                }
            }

            //Button Start (Send to Login or Dashboard if already logged)
            $('#buttonStart').on('click', function(e) {
                // Comprobamos si existe la session
                var stringafeed_key = localStorage.getItem("feed_key");
                var stringafeed_user = localStorage.getItem("feed_user");
                var loginToken = localStorage.getItem("feed_token");
                comprueba_login(stringafeed_key,stringafeed_user,loginToken);
            
                function comprueba_login(a,u,t) {
                    //COMPROBAMOS SI EXISTE LOGIN LOCAL
                    if (t == 0 || t == null) {
                        $.get('templates/init/init-signin.html', function (data) {
                            modal(data)
                        })
                    } else{
                        window.location.href = "dashboard.html";
                    }
                }
            })
        });
    });
})