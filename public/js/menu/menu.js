(function() {
    // Check for click events on the navbar burger icon
    $(".navbar-burger").click(function() {
        $(".navbar-burger").toggleClass("is-active");
        $(".navbar-menu").toggleClass("is-active")
    });
    // SI existe session mandamos el menú de usuario
    compruebaLogin(localStorage.getItem("feed_token"));
}());

function compruebaLogin(t) {
    var content = '<a class="handed navbar-item goTo" data-goto="stop-1">';
        content += 'Como funciona';
    content += '</a>';
    content += '<a class="handed navbar-item goTo" data-goto="stop-2">';
        content += 'Ahorra';
    content += '</a>';
    content += '<a class="handed navbar-item goTo" data-goto="stop-3">';
        content += 'Quienes Somos';
    content += '</a>';
    if (t == 0 || t == null) {
        content += '<a href="" class="navbar-item buttonSignup">';
            content += 'Crea tu Cuenta';
        content += '</a>';
        content += '<a href="" class="navbar-item buttonLogin button is-rounded is-success is-outlined">';
            content += 'Inicia Sesi&oacute;n';
        content += '</a>'
    } else {
        content += '<a class="navbar-item button is-rounded is-success is-outlined" href="dashboard.html">';
            content += '<span class="icon"><i class="fa-solid fa-gauge"></i></span>';
            content += '<span>Dashboard</span>';
        content += '</a>';
        content += '<a href="" class="navbar-item buttonLogout button is-rounded is-danger is-outlined">';
            content += 'Cierra Sesi&oacute;n';
        content += '</a>'
    }
    $('#menu-buttons').html(content)
    // Navigator goto
    $('.goTo').on('click', function(e) {
        e.preventDefault();
        var element = document.querySelector('#' + $(this).data('goto'));
        // smooth scroll to element and align it at the bottom
        element.scrollIntoView({ behavior: 'smooth', block: 'end'})
    })
  
    // Carga Login
    $('.buttonLogin').on('click', function(e) {
        e.preventDefault()
        $.get('templates/init/init-login.html', function (data) {
            modal(data)
        })
    })
    // Carga Sign up
    $('.buttonSignup').on('click', function(e) {
        e.preventDefault()
        $.get('templates/init/init-signin.html', function (data) {
            modal(data);
        })
    })
    // Login out
    $('.buttonLogout').on('click', function(e) {
        e.preventDefault();
        localStorage.removeItem("feed_key");
        localStorage.removeItem("feed_user");
        localStorage.removeItem("feed_token");
        modal('<h5 class="p-4 has-text-right">cerrando sesi&oacute;n...&nbsp;<i class="fa-solid fa-person-walking-arrow-right"></i></h5>');
        setTimeout(location.reload.bind(location), 1000);
    })
}
   