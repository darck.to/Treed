// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAh6qh17x1CDFblS7aP-pdObzs4eJtbnmk",
  authDomain: "cedar-abacus-352420.firebaseapp.com",
  projectId: "cedar-abacus-352420",
  storageBucket: "cedar-abacus-352420.appspot.com",
  messagingSenderId: "1081148514095",
  appId: "1:1081148514095:web:cb97ad1c3cc79093a54bda",
  measurementId: "G-CRH5ES9LJL"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);