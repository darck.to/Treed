$(function() {
    // Menu
    $('#menu').load('templates/menu/menu.html', function(e) {

        // Template
        $('#principalTemplate').load('templates/principal/principal.html', function(e) {

            // Crea números aleatorios para las imagenes
            $('.rndmzrImg').each(function (index, element) {
                let original = $(element).attr('src');
                original = original + "?" + performance.now();
                $(element).attr('src', original)
            });

            // Carga gráfica no.1
            const ctx = document.getElementById('myChart').getContext('2d');
            const myChart = new Chart(ctx, {
                data: {
                    datasets: [{
                        type: 'bar',
                        label: 'Crecimiento',
                        data: [1, 1.2, 1.5, 1.8, 2.2, 3, 3.1, 3.2, 3.5, 3.8, 4.2, 5],
                        backgroundColor: [
                            'rgb(57, 210, 54, 0.5)',
                            'rgb(57, 210, 54, 0.55)',
                            'rgb(57, 210, 54, 0.70)',
                            'rgb(57, 210, 54, 0.75)',
                            'rgb(57, 210, 54, 0.90)',
                            'rgb(57, 210, 54, 0.95)'
                        ],
                        borderColor: [
                            'rgb(57, 210, 54, 1)',
                            'rgb(57, 210, 54, 1)',
                            'rgb(57, 210, 54, 1)',
                            'rgb(57, 210, 54, 1)',
                            'rgb(57, 210, 54, 1)',
                            'rgb(57, 210, 54, 1)'
                        ],
                        borderWidth: 4
                    }, {
                        type: 'line',
                        label: 'Rendimiento',
                        data: [1.2, 1.5, 1.8, 1.9, 3, 3.2, 3.3, 3.5, 3.8, 4.2, 5, 5.2],
                        tension: 0.1,
                        backgroundColor: [
                            'rgb(57, 210, 54, 0.5)',
                            'rgb(57, 210, 54, 0.55)',
                            'rgb(57, 210, 54, 0.60)',
                            'rgb(57, 210, 54, 0.65)',
                            'rgb(57, 210, 54, 0.70)',
                            'rgb(57, 210, 54, 0.75)'
                        ],
                        borderColor: [
                            'rgb(57, 210, 54, 1)',
                            'rgb(57, 210, 54, 1)',
                            'rgb(57, 210, 54, 1)',
                            'rgb(57, 210, 54, 1)',
                            'rgb(57, 210, 54, 1)',
                            'rgb(57, 210, 54, 1)'
                        ],
                        borderWidth: 2
                    }],
                    labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: false
                        }
                    }
                }
            });

            // Input test update chart
            $('#cuantInput').keypress(function(event) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == '13' && $.isNumeric($(this).val())) {
                    calculaTable($(this).val())
                }
            }) // Boton de calculo
            $('#calcInput').on('click', function(event) {
                if ($.isNumeric($('#cuantInput').val())) {
                    calculaTable($(this).val())
                }
            })
            // Crea la table
            function calculaTable(n) {
                let opcion = $('input[name="typeInput"]:checked').val();
                let i = 0, inversion = $('#cuantInput').val();
                let txt = '<div class="box" style="overflow-x:auto;">'
                    txt += '<h2>Resultados</h2>';
                    txt += '<table class="table is-striped is-hoverable is-fullwidth">';
                    let meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
                    let capital = [], utilidades = [];
                    txt += '<tr>';
                        txt += '<th><small>Periodo</small></th>';
                    $.each(meses, function (id, value) {
                        txt += '<th>' + value + '</th>'
                    })
                    txt += '</tr>'
                    while (i < 12) {
                        capital.push(Math.round(inversion));
                        utilidades.push(Math.round((inversion)*opcion));
                        inversion = inversion * opcion;
                        // Table data
                        myChart.data.datasets[0].data[i] = Math.round(inversion);
                        myChart.data.datasets[1].data[i] = (Math.round(inversion)*opcion);
                        i++
                    }
                    // Capital
                    txt += '<tr>';
                        txt += '<th><small>Capital</small></th>';
                    $.each(capital, function (id,value) {   
                        txt += '<td>' + milesNumber(value) + '</td>'
                    })
                    txt += '</tr>';
                    // Utilidades
                    txt += '<tr>'
                        txt += '<th><small>Utilidades</small></th>';
                    $.each(utilidades, function (id,value) {   
                        txt += '<td>' + milesNumber(value) + '</td>'
                    })
                    txt += '</tr>';
                    txt += '</table>';
                txt += '</div>'
                // Add a table
                $('#chartTable').html(txt);
                //myChart.data.datasets[0].data[2] = $(this).val();
                myChart.update()
            }
            // Revisar si es miles
            function milesNumber(n) {
                if (typeof n === 'number') {
                    n += '';
                    var x = n.split('.');
                    var x1 = x[0];
                    var x2 = x.length > 1 ? '.' + x[1] : '';
                    var rgx = /(\d+)(\d{3})/;
                    while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + ',' + '$2');
                    }
                    return x1 + x2;
                } else {
                    return n;
                }
            }


            //Button Start (Send to Login or Dashboard if already logged)
            $('#buttonStart').on('click', function(e) {
                // Comprobamos si existe la session
                var stringafeed_key = localStorage.getItem("feed_key");
                var stringafeed_user = localStorage.getItem("feed_user");
                var loginToken = localStorage.getItem("feed_token");
                comprueba_login(stringafeed_key,stringafeed_user,loginToken);
            
                function comprueba_login(a,u,t) {
                    //COMPROBAMOS SI EXISTE LOGIN LOCAL
                    if (t == 0 || t == null) {
                        $.get('templates/init/init-signin.html', function (data) {
                            modal(data)
                        })
                    } else{
                        window.location.href = "dashboard.html";
                    }
                }
            })
        });
    });
})