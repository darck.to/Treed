(function() {
    // Title 1 & 2
    $('#title-1').html('Principal');
    $('#title-2').html('Resumen de Cuenta');
    
    // Comprobamos si existe la session
    var a = localStorage.getItem("feed_key");
    var u = localStorage.getItem("feed_user")
    
    cargaTemplate(a,u)
  }());

// Carga Vendedor
function cargaTemplate(a,u) {
  $.ajax({
    type: 'POST',
    url: 'https://treedmng.net/php/dashboard/dashboard-dashboard-carga.php',
    data: {
      auth : a,
      user : u
    },
    async: true,
    dataType : 'json',
    crossDomain: true,
    context: document.body,
    cache: false,
    success: function(data) {
      let txt = '';
      $.each(data, function (index, value) {
        if (value.success) {
          toast(value.message);
          txt += '<div class="control">';
            txt += '<div class="tags has-addons">';
              txt += '<span class="tag is-dark">Tu link de Referidos</span>';
              txt += '<span class="tag is-info"><a href="" class="has-text-light">' + value.ref + '</a></span>';
            txt += '</div>';
          txt += '</div>';
          txt += '<div class="columns">';
            txt += '<div class="column is-4">';
              txt += '<div class="card is-hidden card-register">';
                txt += '<div class="card-header">';
                  txt += '<p class="card-header-title">Registro de Clientes</p>';
                txt += '</div>';
                txt += '<div class="card-content">';
                  txt += '<div class="content registra-content">';
                  txt += '</div>';
                txt += '</div>';
              txt += '</div>';
            txt += '</div>';
          txt += '</div>';
          $('#principal').html(txt)
          if (value.link) {
            $.get('templates/init/init-signin.html', function (data) {
                $('.registra-content').html(data);
                // Personalizamos
                $('.signup-section').removeClass('section');
                $('.reg-title').remove();
                $('.buttonLoginForm').remove();
                $('#r-fat').val(value.ref);
                $('#r-fat').css('pointer-events','none');
                $('#r-fat').css('background','hsl(0, 0%, 86%)');
                //$('.signup-section form').removeAttr('id');
                $('.siteFlag').val('off');
                // Reaparece
                $('.card-register').removeClass('is-hidden')
                // Signbutton
                $('.signButton').on('click', function(e) {
                  $('.signButton').addClass('is-loading');
                  //if ($('.siteFlag').val() == 'off') { return false }
                  e.preventDefault()
                  
                  var formData = new FormData();
                  formData.append('auth',a);
                  formData.append('user', u);
                  formData.append('nom', $('#r-nom').val());
                  formData.append('ape', $('#r-ape').val());
                  formData.append('apm', $('#r-apm').val());
                  formData.append('usr', $('#r-usr').val());
                  formData.append('mai', $('#r-mai').val());
                  formData.append('cel', $('#r-cel').val());
                  formData.append('pas', $('#r-pas').val());
                  formData.append('fat', $('#r-fat').val());
  
                  var request = $.ajax({
                    url: 'https://treedmng.net/php/init/init-register.php',
                    method: 'POST',
                    data: formData,
                    contentType: false,
                    processData: false,
                    async: true,
                    dataType : 'json',
                    crossDomain: true,
                    context: document.body,
                    cache: false,
                  });
                  // handle the responses
                  request.done(function(data) {
                    $.each(data, function (name, value) {
                      if (value.success) {
                        toast(value.message);
                        $('.signButton').removeClass('is-loading');
                        $('#initRegisterUser').trigger('reset')
                      } else {
                        $('.signButton').removeClass('is-loading');
                        toast(value.error)
                      }
                    })
  
                  })
                  request.fail(function(jqXHR, textStatus) {
                    $('.signButton').removeClass('is-loading');
                    console.log(textStatus);
                  })
                  request.always(function(data) {
                    // clear the form
                    //$('#initRegisterUser').trigger('reset');
                  })
              })
            })
          }
        }
      })
    },
    error: function(xhr, tst, err) {
      toast('No se pudo revisar la licencia, contacta soporte')
    }
  })
  }