(function() {
    // Title 1 & 2
    $('#title-1').html('Vendedores');
    $('#title-2').html('Visualización, activación de cuentas y revisión de datos');
    
    // Comprobamos si existe la session
    var a = localStorage.getItem("feed_key");
    var u = localStorage.getItem("feed_user")
    
    cargaClientes(a,u);
  }());

// Carga Vendedor
function cargaClientes(a,u) {
    $.ajax({
      type: 'POST',
      url: 'https://treedmng.net/php/dashboard/dashboard-vendedores-carga.php',
      data: {
        auth : a,
        user : u
      },
      async: true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        let txt = '<div class="box table-container has-background-white">';
        txt += '<table class="table is-fullwidth is-relative" id="datatable">';
        txt += '<thead>';
          txt += '<tr>';
            txt += '<th>Vendedor</th>';
            txt += '<th>Tel&eacute;no</th>';
            txt += '<th>Celular</th>';
            txt += '<th>Email</th>';
            txt += '<th>Inscripci&oacute;n</th>';
            txt += '<th>Accciones</th>';
          txt += '</tr>';
        txt += '</thead>'
        $.each(data, function (name, value) {
          if (value.success == true) {
            let nombre = value.nom + ' ' + value.ape + ' ' + value.apm;
            txt += '<tr class="is-size-7 trows no-cmenu" data-ref="' + value.ref + '" data-name="' + nombre + '">';
              txt += '<td>' + nombre + '</td>';
              txt += '<td><a href="tel:' + value.tel + '">' + value.tel + '</a></td>';
              txt += '<td><a href="tel:' + value.cel + '">' + value.cel + '</a></td>';
              txt += '<td><a href="mailto:' + value.mai + '">' + value.mai + '</a></td>';
              txt += '<td>' + value.fea + '</td>';
              txt += '<td class="has-text-centered"><a href="https://api.whatsapp.com/send?phone=52' + value.cel + '" target="_blank"><i class="fa-brands fa-2x has-text-success fa-whatsapp"></i></a></td>';
            txt += '</tr>'
          } else {
            toast(value.message)
          }
        })
        txt += '</table>';
        txt += '</div>';
        $('#infoTable').html(txt);
        // Tables plugin
        $('#datatable').DataTable({
            "dom": 'Blfrtip',
            "bProcessing": true,
            buttons: [
                'excelHtml5'
            ]
        });
        // Selecciona rows
        $('.table .trows').on('click', function(e) {
          $('.table tr').removeClass('is-selected');
          $(this).toggleClass('is-selected')
        })
        // Menu contextual
        $('.no-cmenu').bind("contextmenu",function(e) {
          let top = e.pageY-400;
          let left = e.pageX-350;
          // Html menu
          let txt = '<div class="box context-menu has-background-white-ter">';
            txt += '<ul>';
              txt += '<li class="is-clickable context-action">Código de Referido: ' + $(this).data('ref') + '</li>';
              txt += '<li class="is-clickable context-action" data-ref="' + $(this).data('ref') + '" data-name="' + $(this).data('name') + '">Referidos</li>';
              txt += '<hr class="p-0 m-0">';
              txt += '<li class="is-clickable has-text-danger">Borrar</li>';
            txt += '</ul>';
          txt += '</div>';
          $(this).append(txt)
          // Show contextmenu
          $('.context-menu').toggle(100).css({
              top: top + "px",
              left: left + "px"
          })
          // Acciones de click del menu contextual
          $('.context-action').on('click', function(e) {
            if ($(this).data('ref')) {
              cargaReferidos($(this).data('ref'), $(this).data('name'))
            }
          });
          return false
        })
        // Borra el contextmenu
        $(document).mouseup(function(e) {
          var container = $('.context-menu')
          if (!container.is(e.target) && container.has(e.target).length === 0) {
            container.remove()
          }
        });
         // disable context-menu from custom menu
        $('.context-menu').bind('contextmenu',function(){
          return false
        })
      },
      error: function(xhr, tst, err) {
        toast('No se pudo revisar la licencia, contacta soporte')
      }
    })
  }

  function cargaReferidos(e,n) {
    $.ajax({
      type: 'POST',
      url: 'https://treedmng.net/php/dashboard/dashboard-vendedores-referidos.php',
      data: {
        ref : e
      },
      async: true,
      dataType : 'json',
      crossDomain: true,
      context: document.body,
      cache: false,
      success: function(data) {
        // Crea espacio para referidos
        $('#dashboard-body').append('<section class="section pr-2 pl-2 pt-3 is-relative" id="subTable"></section>');
        $('#infoTable').delay(50).fadeOut();
        let txt = '<div class="box table-container has-background-white">';
        txt += '<h3>REFERIDOS DE: <b>' + n + ', <small>' + e + '</small></b></h3>'
        txt += '<table class="table is-fullwidth" id="dataReferidos">';
        txt += '<thead>';
          txt += '<tr>';
            txt += '<th>Pago</th>';
            txt += '<th>Cliente</th>';
            txt += '<th>Tel&eacute;no</th>';
            txt += '<th>Celular</th>';
            txt += '<th>Email</th>';
            txt += '<th>Caducidad</th>';
            txt += '<th>Inscripci&oacute;n</th>';
            txt += '<th>Accciones</th>';
          txt += '</tr>';
        txt += '</thead>'
        $.each(data, function (name, value) {
          if (value.success == true) {
            // Flag de pago
            let pay;
            value.pago == "0" ? pay = '<i class="fa-solid fa-xmark has-text-danger"></i>' : pay = '<i class="fa-solid fa-check has-text-success"></i>';
            toast(value.message);
            txt += '<tr class="is-size-7 trows">';
              txt += '<td class="has-text-centered">' + pay + '</td>';
              txt += '<td>' + value.nom + ' ' + value.ape + ' ' + value.apm + '</td>';
              txt += '<td><a href="tel:' + value.tel + '">' + value.tel + '</a></td>';
              txt += '<td><a href="tel:' + value.cel + '">' + value.cel + '</a></td>';
              txt += '<td><a href="mailto:' + value.mai + '">' + value.mai + '</a></td>';
              txt += '<td>' + value.fec + '</td>';
              txt += '<td>' + value.fea + '</td>';
              txt += '<td class="has-text-centered"><a href="https://api.whatsapp.com/send?phone=52' + value.cel + '" target="_blank"><i class="fa-brands fa-2x has-text-success fa-whatsapp"></i></a></td>';
            txt += '</tr>'
          } else {
            toast(value.message);
            $('#infoTable').delay(50).fadeIn();
          }
        })
        txt += '</table>';
        txt += '</div>';
        // Agrega Referidos y un botón para cerrar la tabla y regresar a vendedores
        $('#subTable').html(txt);
        $('#subTable').append('<i class="fa-solid fa-circle-xmark fa-2x has-text-danger-dark is-clickable close-dom"></i>');
        // Close dom
        $('.close-dom').on('click', function(e) {
          $(this).parent().remove();
          $('#infoTable').delay(50).fadeIn()
        });
        // Tables plugin
        $('#dataReferidos').DataTable({
            "dom": 'Blfrtip',
            "bProcessing": true,
            buttons: [
                'excelHtml5'
            ]
        });
        // Selecciona rows
        $('.table .trows').on('click', function(e) {
          $('.table tr').removeClass('is-selected');
          $(this).toggleClass('is-selected')
        })
      },
      error: function(xhr, tst, err) {
        toast('No se pudo revisar la licencia, contacta soporte')
      }
    })
  }